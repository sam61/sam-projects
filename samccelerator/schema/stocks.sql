
create table if not exists ticker (
  code      text,
  "name"    text,
  exchange  text,
  "type"    text,
  constraint ticker_pk primary key (code, exchange)
);


create table if not exists stocks (
  ticker    text,
  "time"    timestamptz,
  open      numeric,
  high      numeric,
  low       numeric,
  "close"   numeric,
  volume    numeric,
  constraint stocks_pk primary key (ticker, "time")
);
