pub mod stock_loader;
pub mod samcl;

use std::{env, thread, time};
use std::process::exit;

use stock_loader::{load_tickers, StockLoader, get_ticker_data, Ticker};

use samcl::{trivial, test_platforms};

extern crate ocl;

fn help() {
    println!(r#"
Welcome to samcellerator!
Usage:
    rebuild db-connection-string tickers-path
        -> rebuilds the database
"#);

    exit(1);
}


fn reload_help() {
    println!(r#"
Welcome to samcellerator reload!
Usage:
    rebuild db-connection-string tickers-path eodhd-api-token
        -> db-connection-string: Example postgresql://sam:password@127.0.0.1:5432/stocks
        -> tickers-path:
        -> eodhd-api-token
"#);

    exit(1);
}

fn ticker_load_help() {

    println!(r#"
Welcome to samcellerator continue-loading
Usage:
    rebuild db-connection-string tickers-path
        -> db-connection-string: Example postgresql://sam:password@127.0.0.1:5432/stocks
        -> eodhd-api-token
"#);

    exit(1);
}

fn load_stock_data(api_key: &String, tickers: &Vec<Ticker>, stock_loader: &mut StockLoader) {
    println!("Collecting complete data from API");
    let ten_millis = time::Duration::from_millis(10);
    for ticker in tickers {
        println!("Working on ticker: {}, exchange: {}", ticker.name, ticker.exchange);
        let data = get_ticker_data(ticker, api_key);
        stock_loader.store_daily_data(&data);
        thread::sleep(ten_millis);
    }
}

fn reload_stock_tickers(args: &Vec<String>) {
    
    if args.len() != 5 {
        reload_help();
    }

    let tickers = load_tickers(&args[3]);
    let mut stock_loader = StockLoader::new(&args[2]).unwrap();
    stock_loader.clear_table(&"ticker".to_string());
    stock_loader.clear_table(&"stocks".to_string());
    
    println!("Starting Load");
    stock_loader.store_tickers(&tickers);
    
    println!("Finished Load");
    load_stock_data(&args[4], &tickers, &mut stock_loader);

    exit(0);
}

fn continue_load_stock_tickers(args: &Vec<String>) {

    if args.len() != 4 {
        ticker_load_help();
    }

    let mut stock_loader = StockLoader::new(&args[2]).unwrap();
    let tickers = stock_loader.get_unfinished_tickers();
    load_stock_data(&args[3], &tickers, &mut stock_loader);
    exit(0);
}

fn main() {
    println!("Hello, world!");

    trivial();
    test_platforms();


    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        help();  
    }

    println!("{:?}", args);

    match args[1].as_str() {
        "rebuild"   => reload_stock_tickers(&args),
        "continue-loading" => continue_load_stock_tickers(&args),
        _           => help(),
    }
}

