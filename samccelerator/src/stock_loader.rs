use postgres::{Client, NoTls, Row};
use serde::{Serialize, Deserialize};
use serde_json::Value;

use std::fs;

#[derive(Serialize, Deserialize, Debug)]
pub struct Ticker {
    pub code: String,
    pub name: String,
    pub exchange: String,
    pub r#type: String,
}

fn default_daily_exchange_name() -> String{
    "None".to_owned()
}

#[derive(Serialize, Deserialize, Debug)]
pub struct DailyData {
    pub date: String,
    pub open: f64,
    pub high: f64,
    pub low: f64,
    pub close: f64,
    pub adjusted_close: f64,
    pub volume: u64,
    #[serde(default = "default_daily_exchange_name")]
    pub ticker: String,     
}

pub struct StockLoader {
    client: Client,
}



pub fn load_tickers(path: &String) -> Vec<Ticker> {
    
    let contents = fs::read_to_string(path);
    let mut ticker_vec = Vec::new();

    if let Ok(data) = contents {
        
        let _v = match serde_json::from_str::<Value>(&data.as_str()) {
            Ok(v) => { 
                if let Some(tickers) = v["tickers"].as_array() {
                    for ticker in tickers {
                        //println!("{:?}", serde_json::from_value::<Ticker>(ticker.clone()).unwrap());
                        ticker_vec.push(serde_json::from_value(ticker.clone()).unwrap());
                    }

                }
            },
            Err(e) => {println!("{e}");}

        };

    } else {
        println!("Could not load file");
        panic!("OH NOW");
    }

    ticker_vec
}

pub fn get_ticker_data(ticker: &Ticker, api_token: &String) -> Vec<DailyData> {

    let exchange: String = match ticker.exchange.as_str() {
        "AU" => "AU".to_owned(),
        _ => "US".to_owned(),
    };

    let ticker_name: String = ticker.code.clone() + "." + exchange.as_str();

    let url: String = "https://eodhd.com/api/eod/".to_owned()
        + &ticker_name.as_str()
        + "?api_token=" + api_token.as_str() 
        + "&fmt=json"; 
    
    let body = reqwest::blocking::get(url).unwrap().text().unwrap();

    let mut data = Vec::new();

    let _v = match serde_json::from_str::<Value>(&body.as_str()) {
        Ok(v) => {
            if let Some(daily_data) = v.as_array() {
                for item in daily_data {
                    let mut i: DailyData = serde_json::from_value(item.clone()).unwrap(); 
                    i.ticker = ticker_name.clone();
                    data.push(i);
                }
            } else {
                println!("Something went wrong in daily data");
                panic!("OH NOW");
            }
        },
        Err(e) => {println!("{e}");}
    };

    data
}


impl StockLoader {
    pub fn new(connection_string: &String) -> Result<StockLoader, postgres::Error> {
        let client = open_postgres(connection_string)?; 
        
        Ok(StockLoader {
            client
        })
    }

    pub fn clear_table(&mut self, table: &String) {

        let delete_string: String = "delete from ".to_owned() + table.as_str();

        self.client.execute(
            &delete_string,
            &[]
        ).unwrap();
    }

    pub fn bulk_upload_tickers(&mut self, tickers: &[Ticker]) {
        
        let values: Vec<String> = tickers.into_iter()
            .map( |ticker| format!("('{}', '{}', '{}', '{}')", ticker.code, ticker.name.replace("'", "''"), ticker.exchange, ticker.r#type)).rev().collect();


        let insert_string: String = "insert into ticker (code, name, exchange, type) values ".to_owned() + 
            values.join(", ").as_str();

        // println!("{}", insert_string);
         
        self.client.execute(
            &insert_string,
            &[]
        ).unwrap();

    }

    pub fn store_tickers(&mut self, tickers: &Vec<Ticker>) {
        for ticker_chunk in tickers.chunks(500) {
            self.bulk_upload_tickers(ticker_chunk);
        }
    }

    pub fn bulk_upload_daily_data(&mut self, daily_data: &[DailyData]) {

        let values: Vec<String> = daily_data.into_iter()
            .map( |data| format!("('{}', '{}', {}, {}, {}, {}, {})", data.ticker, data.date, data.open, data.high, data.low, data.close, data.volume)).rev().collect();

        let insert_string: String = "insert into stocks (ticker, time, open, high, low, close, volume) values ".to_owned() +
            values.join(", ").as_str();

        self.client.execute(
            &insert_string,
            &[]
        ).unwrap();
    }

    pub fn store_daily_data(&mut self, daily_data: &Vec<DailyData>) {
        for data_chunk in daily_data.chunks(500) {
            self.bulk_upload_daily_data(data_chunk);
        }
    }

    pub fn get_unfinished_tickers(&mut self) -> Vec<Ticker> {

        let query_string: String = "select * from ticker as t1 where not exists (select 1 from stocks as s1 where s1.ticker = t1.code || '.' || case when t1.exchange = 'AU' then 'AU' else 'US' end);".to_owned(); 

        let mut tickers: Vec<Ticker> = Vec::new();

        for row in self.client.query(&query_string, &[]).unwrap() {
            let ticker: Ticker = Ticker {
                code: row.get("code"),
                name: row.get("name"),
                exchange: row.get("exchange"),
                r#type: row.get("type"),
            };
            
            tickers.push(ticker);
        }
        tickers
    }
}
