use ocl::{ProQue, Platform, Device};


pub fn test_platforms() -> ocl::Result<()> {

    let platforms: Vec<Platform>  = Platform::list();

    for p in platforms {
        println!("{}", p.name().unwrap());
        println!("{}", p.vendor().unwrap());
        println!("{}", p.version().unwrap());

        for d in Device::list(&p, None)? {

            println!("{}", d.name().unwrap());
            println!("{}", d.vendor().unwrap());
            println!("{}", d.version().unwrap());
            println!("{}", d.to_string());
        }
    }

    Ok(())

}

pub fn trivial() -> ocl::Result<()> {
    let src = r#"
        __kernel void add(__global float* buffer, float scalar) {
            buffer[get_global_id(0)] += scalar;
        }
    "#;

    let pro_que = ProQue::builder()
        .src(src)
        .dims(1 << 20)
        .build()?;

    let buffer = pro_que.create_buffer::<f32>()?;

    let kernel = pro_que.kernel_builder("add")
        .arg(&buffer)
        .arg(10.0f32)
        .build()?;

    unsafe { kernel.enq()?; }

    let mut vec = vec![0.0f32; buffer.len()];
    buffer.read(&mut vec).enq()?;

    println!("The value at index [{}] is now '{}'!", 200007, vec[200007]);
    Ok(())
}
