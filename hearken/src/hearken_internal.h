#ifndef HEARKEN_INTERNAL_H
#define HEARKEN_INTERNAL_H 

#include "sds.h"

#ifdef HEARKEN_INTERNAL_KQUEUE

#include "h_kqueue.h"

#elif HEARKEN_INTERNAL_EPOLL

#include "h_epoll.h"

#endif


#include <stdint.h>

typedef struct {
  uint8_t   current_processing;
  uint16_t  port;
  int listening_file_descriptor_ip4;
  int listening_file_descriptor_ip6;
  HearkenPlatformSpecificData* platform_specific_data_handle;
} HHandle;

HearkenPlatformSpecificData* hearken_interface_platform_specific_create();

#endif

