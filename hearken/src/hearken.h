#ifndef HEARKEN_H
#define HEARKEN_H 

#include <stdint.h>
#include <stdlib.h>
#include <string.h>

#include "sds.h"

#ifdef HEARKEN_INTERNAL
  #include "hearken_internal.h"
#else 
  typedef void HHandle;
#endif

/**
 * Creates and destroyes Hearken objects
 *
 * Hearken is a replacement for using either epoll or kqueues directly.
 *
 */

typedef enum {
  READ_READY = 1
} HearkenEvents;

struct HearkenFileDescriptorCallback;  

typedef void HearkenCallbackFunction(
    HHandle* hearken_handle,
    struct HearkenFileDescriptorCallback* hearken_callback_data_handle,
    uint64_t generated_events_bitflag
);

struct HearkenFileDescriptorCallback {
  void* stored_data_handle;
  HearkenCallbackFunction* callback_func; 
}; 

typedef struct HearkenFileDescriptorCallback HearkenFileDescriptorCallback;

HHandle* hearken_library_interface_create();
void hearken_library_interface_delete(HHandle* hearken_handle);

void hearken_interface_attach_file_descriptor(HHandle* hearken_handle, int file_descriptor, HearkenFileDescriptorCallback* callback_data_handle);
HearkenFileDescriptorCallback* hearken_interface_detach_file_descriptor(HHandle* hearken_handle, int file_descriptor, uint8_t set_fd_blocking_bool);

void    hearken_interface_open_listening_socket(HHandle* hearken_handle, uint16_t port);
int32_t hearken_interface_close_listening_socket(HHandle* hearken_handle);
int32_t hearken_interface_start_listening(HHandle* hearken_handle, int32_t max_event_backlog);
int32_t hearken_interface_stop_listening(HHandle* hearken_handle);


#endif
