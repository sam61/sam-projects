#include "h_epoll.h"
#include "hearken_internal.h"


HearkenPlatformSpecificData* hearken_interface_platform_specific_create() {
  
  HearkenPlatformSpecificData* h_platform_data = malloc(sizeof(HearkenPlatformSpecificData));
  
  h_platform_data->epoll_file_descriptor = epoll_create(0);


  if (h_platform_data->epoll_fd == -1) {
    perror("epoll failed to create");
    exit(-1);
  }


  return h_platform_data;
}
