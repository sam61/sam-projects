#include "hearken.h"

int add1() {
  HHandle d;
  
  return 2;
}

HHandle* hearken_library_interface_create() {
  
  HHandle* h_handle = malloc(sizeof(HHandle));

  h_handle->platform_specific_data_handle = hearken_interface_platform_specific_create();
  
  h_handle->port = 0;
  h_handle->current_processing = 0;
  h_handle->listening_file_descriptor_ip4 = 0;
  h_handle->listening_file_descriptor_ip6 = 0;


  return h_handle;
}
