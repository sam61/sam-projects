#pragma once

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

#include <sys/epoll.h>

typedef struct HearkenPlatformSpecificData {
  int epoll_file_descriptor;
} HearkenPlatformSpecificData;

