#ifndef HEARKEN_INTERNAL_H_KQUEUE
#define HEARKEN_INTERNAL_H_KQUEUE

#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <netinet/in.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/event.h>
#include <sys/socket.h>
#include <libc.h>
#include <unistd.h>


// Internal data required for Kqueues
typedef struct HearkenPlatformSpecificData {
  int kqueue_fd; 
} HearkenPlatformSpecificData;

#endif
