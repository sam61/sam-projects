
#include "hearken.h"
#include "hearken_internal.h"


HHandle* hearken_library_interface_create() {
  HearkenPlatformSpecificData* platform_specific_data_handle = malloc(sizeof(HearkenPlatformSpecificData));
  platform_specific_data_handle->kqueue_fd = kqueue();

  HHandle* handle = malloc(sizeof(HHandle));

  // Make sure that the file descriptor is blank
  handle->listening_file_descriptor = 0;

  handle->platform_specific_data_handle = platform_specific_data_handle;
  return handle;
}

void hearken_library_interface_delete(HHandle* hearken_handle) {

  //close kevents
  
  // close connection to outside if it is happening. Basically close quietyly
  free(hearken_handle->platform_specific_data_handle);
  free(hearken_handle);
}



int32_t hearken_interface_start_listening(HHandle* hearken_handle, int32_t max_event_backlog) {

  if (hearken_handle == NULL || hearken_handle->listening_file_descriptor == 0) {
    
    // The listen socket isn't setup yet
    return 1;
  }

  int rc = listen(hearken_handle->listening_file_descriptor, 30);

  if (rc < 0) {
    return rc;
  }

  // register the listenig socket
  struct kevent event;
  EV_SET(&event, hearken_handle->listening_file_descriptor, EVFILT_READ, EV_ADD, 0, 0, NULL);
  kevent(hearken_handle->platform_specific_data_handle->kqueue_fd, &event, 1, NULL, 0, NULL);

  
  // All is working
  return 0;
};

int32_t hearken_interface_close_listening_socket(HHandle* hearken_handle) {

  if (hearken_handle == NULL || hearken_handle->listening_file_descriptor == 0) {
    
    // The listen socket isn't setup yet
    return 1;
  }

  
  int rc  = close(hearken_handle->listening_file_descriptor);
  
  if (rc < 0) {
    return rc;
  }
  

  //Remove the event from kevent

  struct kevent event;
  EV_SET(&event, hearken_handle->listening_file_descriptor, EVFILT_READ, EV_DELETE, 0, 0, NULL);
  kevent(hearken_handle->platform_specific_data_handle->kqueue_fd, &event, 1, NULL, 0, NULL);

  hearken_handle->listening_file_descriptor = 0;

  return 0;
}

void hearken_interface_open_listening_socket(HHandle* hearken_handle, uint16_t port) {

  // We currently have an open port. Need to work out if we just return or rebuild
  if (hearken_handle->listening_file_descriptor > 0) {
    if (port != hearken_handle->port) {
      hearken_interface_close_listening_socket(hearken_handle); 
    } else {
      // Nothing to be done. Socket already is open on these ports
      return;
    }
  }

  //set values
  hearken_handle->port = port;
  
  //create socket
  hearken_handle->listening_file_descriptor = socket(PF_INET, SOCK_STREAM, 0);

  if (hearken_handle->listening_file_descriptor == -1) {
    perror("Listen failed");
    exit(-1);
  }

  // set not block
  const int on = 1;
  int rc = ioctl(hearken_handle->listening_file_descriptor, FIONBIO, (char*)&on);

  if (rc < 0) {
    perror("ioctl failed");
    exit(-2);
  }

  // Set address and bind it to listen socket
  struct sockaddr_in address;
  memset(&address, 0, sizeof(address));

  address.sin_family = AF_INET;
  address.sin_port = htons(hearken_handle->port); 
  address.sin_addr.s_addr = INADDR_ANY;

  rc = bind(hearken_handle->listening_file_descriptor, (const struct sockaddr *)&address, sizeof(address));

  if (rc < 0) {
    perror("Bind failed");
    exit(-3);
  }
  
  // made address and port re-usable);
  rc = setsockopt(hearken_handle->listening_file_descriptor, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on));

  if (rc < 0) {
    perror("set socketopt failed");
    exit(-4);
  }
}
