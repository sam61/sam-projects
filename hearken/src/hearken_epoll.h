#include <stdio.h>

#include <sys/epoll.h>

typedef struct HearkenPlatformSpecificData {
  int epollfd;
} HearkenPlatformSpecificData;

int mul(int a, int d);
