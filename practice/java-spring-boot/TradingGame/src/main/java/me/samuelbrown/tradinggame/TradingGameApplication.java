package me.samuelbrown.tradinggame;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.ModelAndView;

@RestController
@SpringBootApplication
public class TradingGameApplication {

    @RequestMapping("/")
    public String root() {
        return "HELLO!";
    }

    public static void main(String[] args) {
        SpringApplication.run(TradingGameApplication.class, args);
    }



    @RequestMapping("/register")
    @ResponseBody
    public ModelAndView register() {
        ModelAndView modelAndView = new ModelAndView();
        modelAndView.setViewName("register.html");
        return modelAndView;
    }
}
