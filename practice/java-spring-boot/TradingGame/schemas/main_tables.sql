begin;
create table user_metadata (
    id serial primary key,
    username varchar not null,
    money numeric(12,3) not null default 0,
    created timestamptz  not null default 'NOW'::timestamptz,
    passkey varchar not null
);

create table user_money_changes (
    user_id integer not null references user_metadata(id),
    time timestamptz not null default 'NOW'::timestamptz,
    money_change numeric(12,3) not null
);

create table login_logs (
    user_id integer not null references user_metadata(id),
    login timestamptz not null default 'NOW'::timestamptz
);

create table stock (
    id serial primary key,
    stock_name varchar not null,
    ticker_code char(3) not null,
    price numeric(12,3) not null
);

create table user_stock (
    id serial not null primary key,
    user_id integer not null references user_metadata(id),
    stock_id integer not null references stock(id),
    quantity integer not null,
    unique (user_id, stock_id),
    check (quantity >= 0)
);

create table user_stock_history (
    user_stock_id integer not null references user_stock(id),
    time timestamptz not null default 'NOW'::timestamptz,
    quantity_changed integer not null
);

create table stock_price_history (
    stock_id integer not null references stock(id),
    time timestamptz not null default 'NOW'::timestamptz,
    price numeric(12,3) not null,
    primary key (stock_id, price)
);
commit;



