#include <stdlib.h>
#include <iostream>
#include <verilated.h>
#include <verilated_vcd_c.h>

#include "././obj_dir/Vclock_divider.h"
#include "././obj_dir/Vclock_divider___024root.h"

#define MAX_SIM_TIME 20
vluint64_t sim_time = 0;

int main(int argc, char** argv) {
  
  Vclock_divider* cd = new Vclock_divider;

  Verilated::traceEverOn(true);
  VerilatedVcdC *m_trace = new VerilatedVcdC;
  cd->trace(m_trace, 5); 
  m_trace->open("waveform.vcd");

  while (sim_time < MAX_SIM_TIME) {
    cd->clk ^= 1;
    cd->eval();
    m_trace->dump(sim_time);
    sim_time++;
  }

  m_trace->close();
  delete cd;
  exit(EXIT_SUCCESS);
}
