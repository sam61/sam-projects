
// In -> HTML page, out -> SQL statements file
//
// transaction
//
// delete delete from table
// insert into table valuess () ad infinium
//
// end transaction
//
//
// Essentially: 
//  1. get website, 
//  2. store in file
//  3. load file
//  4. fill out main currency [AUD -> everything]
//  5. Then do the same for each exchange rate
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <sys/mman.h>
#include <sys/stat.h>
#include <fcntl.h>


struct exchange_rate {
  char    from_currency[4];
  char    to_currency[4];
  double  exchange_rate;
};


void usage() {
  printf("Usage:\ngxr [options] file\n\t -h\t help\n\t -c XXX\t 3 letter currency abbreviation to start with\n");
}

int process_td_to_exchange_rate(char* current_html_ptr, struct exchange_rate* exchange_rate_ptr) {

  //char hint[1000] = "<td class='rtRates'><a href='https://www.x-rates.com/graph/?from=AUD&amp;to=ARS'>225.716479</a></td>";
  // Helper source: https://stackoverflow.com/questions/24483075/input-using-sscanf-with-regular-expression
  int found = sscanf(
      current_html_ptr, 
      "%*[^\?]?from=%[^&]%*[^=]=%[^\']'>%lf", 
      exchange_rate_ptr->from_currency, 
      exchange_rate_ptr->to_currency, 
      &(exchange_rate_ptr->exchange_rate)
  );

  return found;
}

// Get next position
char* progress_to_next_requested_substring(char* current_pos, char* to_find) {
  int to_find_length = strlen(to_find);
  current_pos = strstr(current_pos, to_find);
  return (current_pos + to_find_length);
}


int get_next_exchange_rate(char* current_pos, struct exchange_rate* exchange_rate_ptr) {
  int found = process_td_to_exchange_rate(current_pos, exchange_rate_ptr);
  if (!found) {
    return 0;
  }
  return 1;
}

int create_initial_exchange_rates(char* current_html_ptr, struct exchange_rate* exchange_rate_ptr) {
  int count = 0;
  while (1) {
    struct exchange_rate er;

    int found = exchange_rate_ptr != NULL ? 
      process_td_to_exchange_rate(current_html_ptr, &exchange_rate_ptr[count]) : 
      process_td_to_exchange_rate(current_html_ptr, &er);

    if (!found) {
      break;
    }

    count += 1;
    current_html_ptr = progress_to_next_requested_substring(current_html_ptr, "tr");
    current_html_ptr = progress_to_next_requested_substring(current_html_ptr, "/tr");
  }
  
  return count;

}

// Loop through and create every single combination of exchange rates
void create_secondary_exchange_rates(struct exchange_rate* all_exchange_rates, int count) {
  
  int insert_position = count;
  for (int i = 0; i < count - 1; i++) {
    for (int j = i + 1; j < count; j++) {
      strncpy(all_exchange_rates[insert_position].from_currency, all_exchange_rates[i].to_currency, 3);
      strncpy(all_exchange_rates[insert_position].to_currency, all_exchange_rates[j].to_currency, 3);
      all_exchange_rates[insert_position].exchange_rate = all_exchange_rates[j].exchange_rate / all_exchange_rates[i].exchange_rate;
      insert_position += 1;
    }
  }
}

void export_sql(FILE* sql_fd, struct exchange_rate* all_exchange_rates, int total_exchange_rate_count) {
  //We need to print out the first portion
  fprintf(sql_fd, "START TRANSACTION;\n");
  fprintf(sql_fd, "DELETE FROM EXCHANGE_RATES;\n");
  fprintf(sql_fd, "INSERT INTO EXCHANGE_RATES(FROM_CUR, TO_CUR, EXCHANGE_RATE) VALUES\n");

  // output the file
  for (int i = 0; i < total_exchange_rate_count; i++) {
    fprintf(
        sql_fd,
        "('%s', '%s', %15.8lf),", 
        all_exchange_rates[i].from_currency, 
        all_exchange_rates[i].to_currency, 
        all_exchange_rates[i].exchange_rate
    );

    fprintf(
        sql_fd,
        "('%s', '%s', %15.8lf)", 
        all_exchange_rates[i].to_currency, 
        all_exchange_rates[i].from_currency, 
        1/all_exchange_rates[i].exchange_rate
    );
    if (i == total_exchange_rate_count - 1) {
      fprintf(sql_fd, ";\n");
    } else {
      fprintf(sql_fd, ",\n");
    }
  }
  fprintf(sql_fd, "COMMIT;\n");
  // Ensure we have written it out
  fflush(sql_fd);
}

// Ensure that the currencies are correctly formatted for our use
int check_user_chosen_currency(char* currency_code) {
  
  //3 letters
  if (strlen(currency_code) != 3) {
    return 1;
  }

  // all caps and letters
  for (int i = 0; i < 3; i++) {
    if (currency_code[i] < 65 || currency_code[i] > 90) {
      return 1;
    }
  }

  return 0;
}

void get_opts(int argc, char** argv, char* currency_code) {
  int opt;


  if (argc < 2) {
    usage();
    exit(0);
  }

  while((opt = getopt(argc, argv, "hc:")) != -1) {
    switch(opt) {
      case 'h':
        usage();
        exit(0);
      case 'c':

        printf("Opt arg: %s\n", optarg);

        if (check_user_chosen_currency(optarg)) {
          usage();
          exit(1);
        }
        strncpy(currency_code, optarg, 3);
        break;
      default:
        usage();
        exit(0);
    }
  }

  // Make sure the last arguement sent was not the currency code
  if (!strcmp(argv[argc-1], currency_code)) {
    usage();
    exit(0);
  }
}

void cleanup(
    FILE* sql_fd, 
    int wget_fd, 
    char* file_data, 
    int file_size, 
    struct exchange_rate* all_exchange_rates
) {

  if (all_exchange_rates != NULL) {
    free(all_exchange_rates);
  }

  if (file_data != NULL) {
    munmap(file_data, file_size);
  }

  if (wget_fd) {
    close(wget_fd);
    remove("/tmp/rates.txt");
  }
  
  if (sql_fd != NULL) {
    fclose(sql_fd);
  }

}


int main(int argc, char** argv) {
  
  //get opts
  char currency_code[4] = "AUD";
  get_opts(argc, argv, currency_code);
  
  //create SQL file and enable buffer
  FILE* sql_fd = fopen(argv[argc-1], "w");

  if  (sql_fd == NULL) {
    perror("Was unable to create the file requested");
    return 1;
  }

  if(setvbuf(sql_fd, NULL, _IOFBF, 1024)) {
    perror("Was unable to create a write buffer for the SQL file");
    cleanup(sql_fd, 0, NULL, 0, NULL);
    return 1;
  }

  // wget the page
  char wget_system_command[100];
  sprintf(wget_system_command, "wget -nv -q -O /tmp/rates.txt \"https://www.x-rates.com/table/?from=%s&amount=1\"", currency_code);
  system(wget_system_command);
  
  // open wget file
  int wget_fd = open("/tmp/rates.txt", O_RDONLY);
  if (wget_fd < 0) {
    perror("Unable to open HTML file retrieved by wget");
    cleanup(sql_fd, 0, NULL, 0, NULL);
    return 1;
  }

  struct stat file_info;
  if (fstat(wget_fd, &file_info) == -1) {
    perror("Unable to fstat the file retrieved by wget");
    cleanup(sql_fd, wget_fd, NULL, 0, NULL);
    return 1;
  }

  off_t file_size = file_info.st_size;

  char *file_data = mmap(NULL, file_size, PROT_READ, MAP_PRIVATE, wget_fd, 0);
  
  if (file_data == MAP_FAILED) {
    perror("Was unable to map the file into memory");
    cleanup(sql_fd, wget_fd, NULL, 0, NULL);
    return 1;
  }

  // Loaded, now need to find stuff
  char* found_pos = strstr(file_data, "Alphabetical order");
  if (found_pos == NULL) {
    perror("Unable to parse contents of wget. Likely an incorrect currency code");
    cleanup(sql_fd, wget_fd, file_data, file_size, NULL);
    return 1;
  }

  found_pos = strstr(found_pos, "tbody");
 
  //We need to know how many rows are in the alphabetical order.
  int count = create_initial_exchange_rates(found_pos, NULL);
  // We need to know how many exchanges in total that will be needed
  int total_exchange_rate_count = (count * count + count) / 2; // Triangle Number
  struct exchange_rate* all_exchange_rates = calloc(total_exchange_rate_count, sizeof(struct exchange_rate));

  if (all_exchange_rates == NULL) {
   perror("Was unable to malloc the memory needed for this operation"); 
   cleanup(sql_fd, wget_fd, file_data, file_size, NULL);
   return 1;
  }

  //Get initial exchange rates that will form the base of all exchange rates
  create_initial_exchange_rates(found_pos, all_exchange_rates);
  //create corresponding exchange rates
  create_secondary_exchange_rates(all_exchange_rates, count);

  // Export SQL
  export_sql(sql_fd, all_exchange_rates, total_exchange_rate_count);
  
  cleanup(sql_fd, wget_fd, file_data, file_size, all_exchange_rates);
  // Cleanup
  return 0;
}
