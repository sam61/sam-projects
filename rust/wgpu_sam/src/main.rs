// The value of `dylib = "..."` should be the library containing the hot-reloadable functions
// It should normally be the crate name of your sub-crate.
#[hot_lib_reloader::hot_module(dylib = "display")]
mod hot_lib {
    // Reads public no_mangle functions from lib.rs and  generates hot-reloadable
    // wrapper functions with the same signature inside this module.
    // Note that this path relative to the project root (or absolute)
    hot_functions_from_file!("display/src/lib.rs");

    // Because we generate functions with the exact same signatures,
    // we need to import types used
    pub use display::State;
}
#[tokio::main]
async fn main() -> std::io::Result<()>  {
    println!("HELLO1");
    let mut state = hot_lib::new_display();
    println!("HELLO2");
    hot_lib::setup(&mut state);
    println!("HELLO3");
    // Running in a loop so you can modify the code and see the effects
    loop {
        println!("Hmmm");
        hot_lib::print_events(&mut state).await;
        std::thread::sleep(std::time::Duration::from_secs(1));
    }

}
