
use std::{io::stdout, time::Duration, io::Stdout};

use futures::{future::FutureExt, select, StreamExt};
use futures_timer::Delay;

use crossterm::{
    execute,
    cursor::position,
    event::{DisableMouseCapture, EnableMouseCapture, Event, EventStream, KeyCode},
    terminal::{disable_raw_mode, enable_raw_mode}
};

pub struct State {
    pub counter: usize,
    reader: EventStream,
    stdout: Stdout,
    
}

#[no_mangle]
pub fn step(state: &mut State) {
    state.counter += 1;
    println!("doing stuff in iteration. Sam is cool yay! {}", state.counter);
}


#[no_mangle]
pub fn new_display() -> State {
    State {
        counter: 0,
        reader: EventStream::new(),
        stdout: stdout()
    }
}


#[no_mangle]
pub fn setup(state: &mut State) -> std::io::Result<()>  {
    
    enable_raw_mode()?;
        
    execute!(state.stdout, EnableMouseCapture)?;

    return Ok(())
}

pub fn tear_down(state: &mut State) -> std::io::Result<()> {
        
    execute!(state.stdout, DisableMouseCapture)?;

    disable_raw_mode()
}

#[no_mangle]
pub async fn print_events(state: &mut State) {
    
    println!("HELLO4");
    loop {

        println!("HELLO5");
        let mut delay = Delay::new(Duration::from_millis(1_000)).fuse();
        let mut event = state.reader.next().fuse();

        println!("HELLO6");
        
        select! {
            _ = delay => { println!(".\r"); },
            maybe_event = event => {
                match maybe_event {
                    Some(Ok(event)) => {
                        println!("Event::{:?}\r", event);

                        if event == Event::Key(KeyCode::Char('c').into()) {
                            println!("Cursor position: {:?}\r", position());
                        }

                        if event == Event::Key(KeyCode::Esc.into()) {
                            break;
                        }
                    }
                    Some(Err(e)) => println!("Error: {:?}\r", e),
                    None => break,
                }
            }
        };
    }

}
