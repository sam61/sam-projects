use std::path::Path;

use rusqlite::{Connection, Result};

use crate::storage::common::stock::Stock;
use crate::storage::common::tick::Tick;
use crate::storage::common::page::{Paginated, PageRequest};
use crate::storage::ports::inbound::stocks_use_case::{GetStocks, GetTicks, SaveData};

#[derive(Debug)]
pub struct SqliteDb {
    client: Connection,
}


const CREATE_STOCK_TABLE: &str = r#"
    CREATE TABLE stock {
        code TEXT,
        name TEXT,
        exchange TEXT,
        type TEXT,
        PRIMARY KEY (code, exchange)
    };
"#;

const CREATE_TICK_TABLE: &str = r#"
    CREATE TABLE tick {
        stock TEXT,
        time TEXT,
        open REAL,
        high REAL,
        low REAL,
        close REAL,
        volume NUMERIC,
        PRIMARY KEY (stock, time),
        FOREIGN KEY (stock)
            references stock (code)
                on delete cascade
                on update no action
    };

"#;

const INSERT_STOCK_TABLE: &str = r#"
    INSERT INTO TABLE stock(code, name, exchange, type)
    VALUES 
"#;

const INSERT_TICK_TABLE: &str = r#"
    INSERT INTO TABLE tick
        (stock, timestamp, open, high, low, close, volume)
    VALUES 
"#;

impl SqliteDb {
    
    fn open_existing_db(path: &String) -> Result<SqliteDb> {
        let client = Connection::open(path)?;

        Ok(SqliteDb {
            client
        })
    }


    fn open_new_db(path: &String) -> Result<SqliteDb> {
        let client = Connection::open(path)?;

        let db = SqliteDb {
            client
        };

        db.execute(&CREATE_STOCK_TABLE.to_owned());
        db.execute(&CREATE_TICK_TABLE.to_owned());

        Ok(db)
    }

    pub fn execute(&self, sql: &String) {
        match self.client.execute(sql, ()) {
            Ok(updated) => println!("{} rows were updated", updated),
            Err(err) => println!("update failed: {}", err),
        }
    }

    /// Opens an SQliteDb Connection with the given path
    ///
    /// If the DB is new it will intialize the DB Tables
    pub fn new(path: &String) -> Result<SqliteDb>  {
       if Path::new(path).exists() {
            return SqliteDb::open_existing_db(path)
       } else {
            return SqliteDb::open_new_db(path);
       }
    }
}

impl GetStocks for SqliteDb {

    fn get_stock(
        &self,
        code: &String
    ) -> Option<Stock> {
        
        None
    }

    fn get_stocks_without_data(
        &self,
        exchange: Option<String>,
        stock_type: Option<String>,
        page_request: PageRequest,
    ) -> Paginated<Stock> {
        todo!()
    }
    
    fn get_stocks(
        &self,
        exchange: Option<String>,
        stock_type: Option<String>,
        page_request: PageRequest,
    ) -> Paginated<Stock> {
        todo!()
    }
}


fn tick_value(tick: &Tick) -> String {
    format!("('{}', '{}', {}, {}, {}, {}, {})",
        tick.stock,
        tick.timestamp,
        tick.open,
        tick.high,
        tick.low,
        tick.close,
        tick.volume
    )
}

fn bulk_upload_ticks(ticks: &[Tick]) -> String {

    let values: Vec<String> = ticks.into_iter()
        .map(tick_value)
        .collect();


    INSERT_TICK_TABLE.to_owned() +
        values.join(", ").as_str()
}

fn stock_value(stock: &Stock) -> String {
    format!("('{}','{}','{}','{}')",
        stock.code,
        stock.name.replace("'", ""),
        stock.exchange,
        stock.stock_type
    )
}

fn bulk_upload_stock(stocks: &[Stock]) -> String {
    let values: Vec<String> = stocks.into_iter()
        .map(stock_value)
        .collect();

    INSERT_STOCK_TABLE.to_owned() +
        values.join(", ").as_str()
}

impl SaveData for SqliteDb {

    fn save_stocks(
        &self,
        stocks: &Vec<Stock>
    ) -> Result<(), String> {
        for stock_chunk in stocks.chunks(100) {
            let sql = bulk_upload_stock(stock_chunk);
            self.execute(&sql);
        }
        Ok(())
    }
    
    fn save_ticks(
        &self,
        ticks: &Vec<Tick>
    ) -> Result<(), String> {
        for tick_chunk in ticks.chunks(100) {
            let sql = bulk_upload_ticks(tick_chunk);
            self.execute(&sql);
        }
        Ok(())
    }
}












