use postgres::{Client, NoTls};

pub struct PostgresStockDb {
    client: Client
}

fn open_postgres(connection_string: &String) -> Result<Client, postgres::Error> {
    Client::connect(connection_string, NoTls)
}

impl PostgresStockDb {
    pub fn new(connection_string: &String) 
        -> Result<PostgresStockDb, postgres::Error> {
        Ok(PostgresStockDb {
            client: open_postgres(connection_string)?
        })
    }
}


