
pub struct PageRequest {
    pub page: i64,
    pub page_size: i64,
}

pub struct Paginated<T> {
    pub page: i64,
    pub page_size: i64,
    pub count: i64,
    pub data: Vec<T>
}
