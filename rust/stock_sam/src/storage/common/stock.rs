pub struct Stock {
    pub code: String,
    pub name: String,
    pub exchange: String,
    pub stock_type: String,
}
