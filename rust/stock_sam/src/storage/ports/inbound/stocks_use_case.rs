use std::collections::HashMap;

use crate::storage::common::page::{Paginated, PageRequest};
use crate::storage::common::stock::Stock;
use crate::storage::common::tick::Tick;

pub trait GetStocks {
    
    fn get_stock(
        &self,
        code: &String
    ) -> Option<Stock>;
    
    fn get_stocks(
        &self,
        exchange: Option<String>,
        stock_type: Option<String>,
        page_request: PageRequest,
    ) -> Paginated<Stock>;

    fn get_stocks_without_data(
        &self,
        exchange: Option<String>,
        stock_type: Option<String>,
        page_request: PageRequest,
    ) -> Paginated<Stock>;
}

pub trait GetTicks {
    
    fn get_specific_stock_ticks(
        &self,
        code: &String,
        between_timestamps: (&String, &String),
        page_request: PageRequest
    ) -> Option<Paginated<Tick>>;
    
    fn get_stock_ticks_between_time(
        &self,
        exchange: Option<&String>,
        stock_type: Option<&String>,
        between_timestamps: (&String, &String),
        page_request: PageRequest
    ) -> Option<Paginated<HashMap<String, Vec<Tick>>>>;

}


pub trait SaveData {
 

    fn clear_stocks(&self) {

    }

    fn save_stocks(
        &self,
        stocks: &Vec<Stock>
    ) -> Result<(), String>;
    
    fn save_ticks(
        &self,
        ticks: &Vec<Tick>
    ) -> Result<(), String>;
}
