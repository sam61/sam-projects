use std::process::exit;


pub mod algo;
pub mod collection;
pub mod reports;
pub mod storage;

// My hemmingway path for tomorrow is to get 
// the postgres ports going again

// Another one is to see if there an EPS thingy
// and Latex library

fn help() {
    println!("I am a help message. I am not written yet.");
    exit(-1);
}

fn new_collection(args: &Vec<String>) {



}

fn main() {

    let args: Vec<String> = std::env::args().collect();

    if args.len() < 2 {
        help();
    }

    match args[1].as_str() {
        "new"   => new_collection(&args),
        _       => help()
    }
}
