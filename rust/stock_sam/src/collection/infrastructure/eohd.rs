
use serde::de::DeserializeOwned;
use serde::{Serialize, Deserialize};
use serde_json::Value;

use crate::collection::ports::outbound::
    {CollectStockDataUseCase, GetFromExchangesUseCase};
use crate::storage::common::stock::Stock;
use crate::storage::common::tick::Tick;

pub struct Eohd {
    api_key: String,
    base_url: String
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Ticker {
    pub code: String,
    pub name: String,
    pub country: String,
    pub exchange: String,
    pub r#type: String,
    pub isin: Option<String>
}

fn default_daily_exchange_name() -> String{
    "None".to_owned()
}

#[derive(Serialize, Deserialize, Debug)]
pub struct DailyData {
    pub date: String,
    pub open: f64,
    pub high: f64,
    pub low: f64,
    pub close: f64,
    pub adjusted_close: f64,
    pub volume: u64,
    #[serde(default = "default_daily_exchange_name")]
    pub ticker: String,     
}

impl Eohd {
    pub fn new(api_key: String, base_url: String) -> Eohd {
        Eohd {
            api_key,
            base_url
        }
    }

    fn call(&self, path: &String, additional_params: &Vec<String>) -> Result<String, reqwest::Error> {
        
        let additional_params_add = if additional_params.len() > 0 {"&"} else {""};

        let url: String = path.to_owned() 
            + "?api_token=" + &self.api_key
            + additional_params_add 
            + &additional_params.join("&")
            + "&fmt=json";
        
        Ok(reqwest::blocking::get(url)?.text()?)
    }

    fn call_and_convert<T: DeserializeOwned, U>(
        &self, 
        url: String, 
        additional_params: &Vec<String>, 
        convert: fn(T) -> U 
    ) -> Result<Vec<U>, String> {
        match self.call(&url, additional_params) {
            Ok(body) => {
                return Ok(
                    convert_string_to_vec::<T>(body) //serde convert to corresponding type
                    .into_iter()
                    .map(convert)
                    .collect()
                )
            },
            Err(e) => {
                return Err(e.to_string())
            }

        }
    }
}

fn convert_string_to_vec<T: DeserializeOwned>(values: String) -> Vec<T> {
    
    let mut stored_data = Vec::new();

    let _v = match serde_json::from_str::<Value>(&values.as_str()) {
        Ok(v) => {
            if let Some(data) = v.as_array() {
                for item in  data {
                    let i: T = serde_json::from_value(item.clone()).unwrap();
                    stored_data.push(i);
                }
            }
        },
        Err(e) => {println!("{e}");}
    };

    stored_data
}

fn convert_daily_data_to_tick(daily_data: DailyData) -> Tick {
    Tick {
        stock: daily_data.ticker,
        timestamp: daily_data.date,
        open: daily_data.open,
        high: daily_data.high,
        low: daily_data.low,
        close: daily_data.close,
        volume: daily_data.volume


    }
}

fn convert_ticker_to_stock(ticker: Ticker) -> Stock {
    Stock {
       code: ticker.code.to_owned(),
       exchange: ticker.exchange.to_owned(),
       name: ticker.name.to_owned(),
       stock_type: ticker.r#type.to_owned()
    }
}

impl GetFromExchangesUseCase for Eohd {
    
    fn get_stock_list_from_exchange(
        &self,
        exchange: &String
    ) -> Result<Vec<Stock>, String> {
        
        let url = self.base_url.to_owned()
            + "/exchange-symbol-list/" 
            + exchange;

        match self.call(&url, &Vec::new()) {
            Ok(body) => {
                return Ok(
                    convert_string_to_vec::<Ticker>(body)
                    .into_iter()
                    .map( |mut i| {
                        i.exchange = exchange.to_string();
                        convert_ticker_to_stock(i)
                    })
                    .collect()
                )
            },
            Err(e) => {
                return Err(e.to_string())
            }
        };
    }
}

impl CollectStockDataUseCase for Eohd {

    fn get_all_stock_ticks(
        &self,
        stock: &Stock,
    ) -> Result<Vec<Tick>, String> {
        let url = self.base_url.to_owned()
            + "/eod/"
            + &stock.code + "." + &stock.exchange;
        self.call_and_convert::<DailyData, Tick>(
            url, 
            &Vec::new(), 
            convert_daily_data_to_tick
        )
    }


    fn get_daily_stock_ticks_for_exchange(
        &self,
        exchange: &String,
        timestamp: &String
    ) -> Result<Vec<Tick>, String> {
        
        let url = self.base_url.to_owned()
            + "/eod-bulk-last-day/"
            + exchange;
        
        let additional_params = vec!["date=".to_owned() + timestamp];

        self.call_and_convert::<DailyData, Tick>
            (url, &additional_params, convert_daily_data_to_tick)
    }
}






