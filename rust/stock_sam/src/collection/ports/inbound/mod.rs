
pub trait NewStockCollectionCommandsUseCase {
    fn start_new_collection(&self) -> Result<(), String>;
    fn resume_new_collection(&self) -> Result<(), String>;
    fn completed_collection(&self) -> bool;
}
