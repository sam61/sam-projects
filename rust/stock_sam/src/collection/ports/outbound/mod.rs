
use crate::storage::common::stock::Stock;
use crate::storage::common::tick::Tick;

pub trait GetFromExchangesUseCase {

    fn get_stock_list_from_exchange(
        &self,
        exchange: &String
    ) -> Result<Vec<Stock>, String>;

}

pub trait CollectStockDataUseCase {
    
    fn get_all_stock_ticks(
        &self,
        stock: &Stock,
    ) -> Result<Vec<Tick>, String>;


    fn get_daily_stock_ticks_for_exchange(
        &self,
        exchange: &String,
        timestamp: &String
    ) -> Result<Vec<Tick>, String>;

}
