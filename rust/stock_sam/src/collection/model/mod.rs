use crate::storage::ports::inbound::stocks_use_case::{GetStocks, GetTicks, SaveData};
use crate::collection::ports::inbound::NewStockCollectionCommandsUseCase;

const COMMON_EXCHANGES: &'static [&str] =
    &["FOREX", "GBOND", "AU", "TA", "XETRA", "LSE", "US"]; 


pub struct Collection<'a> {
   

    //output
    get_stocks: &'a dyn GetStocks,
    get_ticks:  &'a dyn GetTicks,
    save_data:  &'a dyn SaveData
}


impl Collection<'_> {
    
    fn new<'b>(
        get_stocks: &'b dyn GetStocks,
        get_ticks: &'b dyn GetTicks,
        save_data: &'b dyn SaveData
    ) -> Collection<'b> {
        Collection {
            get_stocks,
            get_ticks,
            save_data
        }
    }

}

impl NewStockCollectionCommandsUseCase for Collection<'_>{
    
    fn start_new_collection(&self) -> Result<(), String> {
        todo!();
    }

    fn resume_new_collection(&self) -> Result<(), String>{
        todo!();

    }

    fn completed_collection(&self) -> bool {
        todo!()

    }
}



