#!/bin/bash

psql -d postgres -c 'drop database if exists neat'
psql -d postgres -c 'create database neat'

psql -d neat -a -f schema/neat.sql

