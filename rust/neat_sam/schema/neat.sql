
begin;

create table neat_instances (
  id        serial primary key,
  title     text,
  created   timestamp with time zone default now()
);

-- 
create table species (
  id              serial primary key,
  neat_id         integer not null references neat_instances(id),   
  "name"          text,
  created         timestamptz not null default now(),
  stagnation_max  integer not null default 0
);

create type node_type as enum (
  'bias',
  'input',
  'output',
  'hidden'
);

create table geneotypes (
  id          serial primary key,
  species_id  integer not null references species(id),
  created     timestamptz not null default now(),
  active      boolean not null default true
);

create table nodes (
  id          serial primary key,
  species_id  integer not null references species(id),
  created     timestamptz not null default now(),
  "type"      node_type not null
);

create table genes (
  id          serial primary key,
  species_id  integer not null references species(id),
  created     timestamptz not null default now(),
  in_node     integer not null references nodes(id),
  out_node    integer not null references nodes(id)
);

create table geneotype_genes (
  id            serial primary key,
  geneotype_id  integer not null references geneotypes(id),
  created       timestamptz not null default now(),
  enabled       boolean not null default true,
  weight        numeric not null
);

-- replaces the species in the documentation
create table breeds (
  id                serial primary key,
  species_id        integer not null references species(id),
  created           timestamptz not null default now(),
  number            integer not null default 0,
  stagnation_count  integer not null default 0
);

create table breed_geneotypes (
  id                serial primary key,
  species_id        integer not null references species(id),
  geneotype_id      integer not null references geneotypes(id),
  created           timestamptz not null default now(),
  active            boolean not null default true
);


create function initial_neat_instance(t text)
returns integer as $$
declare
  neat_id integer;
begin
  insert into neat_instances(title)
  values (t)
  returning id into neat_id;

  return neat_id;
end;
$$ language plpgsql strict;


create function new_species(
  instance_id integer,
  species_name text,
  stagnation_max integer,
  inputs integer,
  outputs integer
) returns integer as $$
declare
  created_species_id integer;
  initial_genotype_id integer;
begin
  insert into species(neat_id, "name", stagnation_max) 
  values (instance_id, species_name, stagnation_max)
  returning id into created_species_id;

  insert into geneotypes (species_id)
  values (created_species_id)
  returning id into initial_genotype_id;

  insert into nodes(species_id, "type")
  select created_species_id as species_id, 'input'::node_type as "type"
  from generate_series(1,4) as x
  union all
  select created_species_id as species_id, 'output'::node_type as "type"
  from generate_series(1,3) as y
  union all
  select created_species_id as species_id, 'bias'::node_type as "type";

  return created_species_id;
end;
$$ language plpgsql strict;


commit;
