
use std::hash::{Hash, Hasher};

#[derive(Debug, Clone, Copy)]
pub enum NodeType {
    Bias,
    Input,
    Output,
    Hidden
}

#[derive(Debug, Clone, Copy)]
pub struct Node {
    pub id: u32,
    pub class: NodeType
}


impl Node {
    
    pub fn new(class: NodeType, node_id: u32) -> Node {
        Node {
            id: node_id,
            class
        }
    }

}

impl PartialEq for Node {
    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}

impl Eq for Node {}

impl Hash for Node {
    
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.id.hash(state);
    }

}
