
use std::hash::{Hash, Hasher};

use oorandom::Rand32;

use super::instance::{InstanceStorage, InstanceParameters};
use super::node::Node;

#[derive(Debug, Clone, Copy)]
pub struct Gene {
    pub innovation: u32,
    pub in_node: u32,
    pub out_node: u32,
    pub weight: f32,
    pub enabled: bool,
    pub parent: Option<u32>
}

impl Gene {
    pub fn new(in_node: u32, out_node: u32, parent: Option<u32>, random_generator: &mut Rand32) -> Gene {
        Gene {
            innovation: 0,
            in_node,
            out_node,
            enabled: true,
            weight: random_generator.rand_float(),
            parent
       }
    }

    pub fn new_with_innovation(in_node: u32, out_node: u32, innovation: Option<u32>, parent: Option<u32>, random_generator: &mut Rand32) -> Gene {
        Gene {
            innovation: innovation.unwrap_or(0),
            in_node,
            out_node,
            enabled: true,
            weight: random_generator.rand_float(),
            parent
       }
    }

    pub fn mutate_weight(&mut self, random_generator: &mut Rand32, parameters: &InstanceParameters) {
       
        if random_generator.rand_float() <= parameters.weight_mutation_new_weight_chance {
            self.weight = random_generator.rand_float();
        } else {
            self.weight += parameters.weight_mutation_perturbed_value;
        }
    }
}

impl PartialEq for Gene {
    fn eq(&self, other: &Self) -> bool {
        self.innovation == other.innovation
    }
}

impl Eq for Gene {
}

impl Hash for Gene {
    fn hash<H: Hasher>(&self, state: &mut H) {
        self.in_node.hash(state);
        self.out_node.hash(state);
    }
}
