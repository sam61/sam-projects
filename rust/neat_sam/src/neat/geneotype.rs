use std::sync::{Arc, RwLock};
use std::collections::HashSet;

use super::node::{Node, NodeType};
use super::gene::Gene;
use crate::neat::instance::InstanceStorage;
use crate::neat::instance::build_new_random_generator;

use oorandom::Rand32;

pub struct Geneotype {
    nodes: HashSet<Node>,
    genes: Vec<Gene>,
    locked_storage: Arc<RwLock<InstanceStorage>>,
    random_generator: Rand32
}

impl Geneotype {

    pub fn build_minimal_viable_geneotype(
        input_nodes: Vec<Node>, 
        output_nodes: Vec<Node>, 
        locked_storage: Arc<RwLock<InstanceStorage>>,
        random_generator: Rand32
    ) -> Geneotype {
        
        let mut geneotype = Geneotype {
            nodes: HashSet::new(),
            genes: Vec::new(),
            random_generator,
            locked_storage: locked_storage.clone()
        };

        let mut storage = locked_storage.write().unwrap();

        for input in &input_nodes {
            for output in &output_nodes {
                let gene = Gene::new(
                    input.id,
                    output.id,
                    None,
                    &mut geneotype.random_generator
                );
                geneotype.genes.push(storage.register_new_gene(gene));
            }
        }
        
        geneotype.nodes.extend(input_nodes);
        geneotype.nodes.extend(output_nodes);
        
        geneotype
    }

    pub fn create_copy(&mut self) -> Geneotype {
        let nodes = self.nodes.iter().map(|&x| x.clone()).collect();
        let genes = self.genes.iter().map(|&x| x.clone()).collect();

        Geneotype {
            nodes,
            genes,
            random_generator: build_new_random_generator(self.random_generator.rand_u32(), self.random_generator.rand_u32()),
            locked_storage: self.locked_storage.clone()
        }
    }

    pub fn mutate_add_connection(&mut self) {
        
        // Pick a random node
        let node_pos = self.random_generator.rand_range(0..(self.nodes.len() as u32)) as usize;
        let node_id = self.nodes.iter().nth(node_pos).unwrap().id;     
        
        // Possible pairings
        let mut matching_nodes: Vec<u32> = Vec::new();

        for node in &self.nodes {
            // If none exist than this is a valid pairing
            if self.genes.iter().filter(|x| x.in_node == node_id && x.out_node == node.id).count() == 0 {
                matching_nodes.push(node.id);
            }
        }

        let out_node_pos = self.random_generator.rand_range(0..(matching_nodes.len() as u32)) as usize;
        let out_node_id = matching_nodes[out_node_pos];

        let mut storage = self.locked_storage.write().unwrap();
        // New gene
        let new_gene = storage.register_new_gene(Gene::new(
            node_id, 
            out_node_id, 
            None, 
            &mut self.random_generator 
        ));

        self.genes.push(new_gene);
    }

    pub fn mutate_split_connection(&mut self) {
        
        // Pick a random connection to split
        let mut enabled_genes: Vec<&mut Gene> = self.genes.iter_mut().filter(|x|  x.enabled).collect();
        let random_gene_pos = self.random_generator.rand_range(0..(enabled_genes.len() as u32)) as usize;
        let mut picked_gene:  &mut Gene = enabled_genes[random_gene_pos];

        let mut storage = self.locked_storage.write().unwrap();

        // Check to see if it is already split.
        // If it is, copy the bits from the storage
        // Else, create them!
        
        let (mut in_gene,  out_gene, node) = storage.get_split_gene(picked_gene).unwrap_or_else(|| {
            let new_node = storage.create_new_node(NodeType::Hidden);

            let new_in_gene = storage.register_new_gene(Gene::new(picked_gene.in_node, new_node.id, Some(picked_gene.innovation), &mut self.random_generator));
            let new_out_gene = storage.register_new_gene(Gene::new(new_node.id, picked_gene.out_node, Some(picked_gene.innovation), &mut self.random_generator));

            (new_in_gene, new_out_gene, new_node)
        });

        picked_gene.enabled = false;
        in_gene.weight = 1.0;
        
        self.genes.push(in_gene);
        self.genes.push(out_gene);
        self.nodes.insert(node);
    }



    pub fn new() {
    }
}

/*impl PartialEq for  Geneotype {

    fn eq(&self, other: &Self) -> bool {
        self.id == other.id
    }
}*/

