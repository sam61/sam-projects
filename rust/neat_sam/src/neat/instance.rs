use std::sync::{Arc, RwLock};
use std::collections::HashSet;

use oorandom::Rand32;

use super::node::{Node, NodeType};
use super::gene::Gene;
use super::geneotype::Geneotype;

pub struct InstanceStorage {
    node_master_list: HashSet<Node>,
    gene_master_list: HashSet<Gene>,
    geneotype_master_list: Vec<Geneotype>,
    next_innovation_number: u32,
    next_node_number: u32
}

impl InstanceStorage {
    
    pub fn new() -> InstanceStorage {
        InstanceStorage {
            node_master_list: HashSet::new(),
            gene_master_list: HashSet::new(),
            geneotype_master_list: Vec::new(),
            next_innovation_number: 0,
            next_node_number: 0
        }
    }

    fn get_next_node_number(&mut self) -> u32 {
        self.next_node_number += 1;
        self.next_node_number
    }

    fn get_next_innovation_number(&mut self) -> u32 {
        self.next_innovation_number += 1;
        self.next_innovation_number
    }

    pub fn create_new_node(&mut self, class: NodeType) -> Node {
        let node = Node::new(class, self.get_next_node_number());
        self.node_master_list.insert(node.clone());
        node
    }

    pub fn get_split_gene(&self, gene: &Gene) -> Option<(Gene, Gene, Node)> {
        let matched_genes: Vec<&Gene> = self.gene_master_list.iter().filter(|x| x.parent.is_some_and(|f| f == gene.innovation)).collect();

        if matched_genes.len() == 0 {
            return None
        }

        // Create tuple, where the first element is where gene in_node matches
        let (in_gene, out_gene) = if matched_genes[0].in_node == gene.in_node 
            {(matched_genes[0], matched_genes[1])} else
            {(matched_genes[1], matched_genes[0])};

        Some((
            in_gene.clone(),
            out_gene.clone(),
            Node {
               id: in_gene.out_node,
               class: NodeType::Hidden
            }
        ))
    }

    pub fn save_geneotype(&mut self, geneotype: Geneotype) {
        self.geneotype_master_list.push(geneotype);
    }

    pub fn register_new_gene(&mut self, mut gene: Gene) -> Gene {
        if self.gene_master_list.contains(&gene) {
            return gene;
        }
       
        match self.gene_master_list.get(&gene) {
            Some(found_gene) => return found_gene.clone(),
            None => {
                gene.innovation = self.get_next_innovation_number();
                self.gene_master_list.insert(gene.clone());
                gene
            }
        }
    }
}

pub struct InstanceParameters {
    pub max_population: u32,
    pub compatibility_coefficient_1: f32,
    pub compatibility_coefficient_2: f32,
    pub compatibility_coefficient_3: f32,
    pub delta_t: f32, //must work this out
    pub maximum_stagnation: u32,
    pub weight_mutation_chance: f32,
    pub weight_mutation_new_weight_chance: f32,
    pub weight_mutation_perturbed_value: f32,
    pub inherited_gene_disabled: f32,
    pub population_created_without_crossover: f32,
    pub interspecies_mating_rate: f32,
    pub node_mutation_rate: f32,
    pub connection_mutation_rate: f32,
}

pub struct Instance {
    storage: Arc<RwLock<InstanceStorage>>,
    rng_generator: Rand32,
    parameters: InstanceParameters
}


impl Instance {
    
    pub fn new(seed: Option<u64>) -> Instance {
        let storage = InstanceStorage::new();

        let parameters = InstanceParameters {
            max_population: 150,
            compatibility_coefficient_1: 1.0,
            compatibility_coefficient_2: 1.0,
            compatibility_coefficient_3: 0.4,
            delta_t: 3.0,
            maximum_stagnation: 15,
            weight_mutation_chance: 0.9,
            weight_mutation_new_weight_chance: 0.1,
            weight_mutation_perturbed_value: 0.05,
            inherited_gene_disabled: 0.75,
            population_created_without_crossover: 0.25,
            interspecies_mating_rate: 0.001,
            node_mutation_rate: 0.003,
            connection_mutation_rate: 0.03,
        };

        Instance {
            storage: Arc::new(RwLock::new(storage)),
            parameters,
            rng_generator: Rand32::new(seed.unwrap_or(4)) // use a generic number for now. Come up
                                                          // with better one later
        }
    }

    fn generate_bulk_nodes(&mut self, number: u32, class: NodeType) -> Vec<Node> {
        
        let mut builder = self.storage.write().unwrap();
        let mut nodes = Vec::new();
        for _ in 0..number {
            nodes.push(builder.create_new_node(class.clone()));
        }

        nodes
    }

    pub fn register_new_instance(&mut self, inputs: u32, outputs: u32, initial_size: Option<u32>) {
        // inputs, outputs
        let mut input_nodes = self.generate_bulk_nodes(inputs, NodeType::Input);
        input_nodes.push(self.generate_bulk_nodes(1, NodeType::Bias)[0].clone());
        let output_nodes = self.generate_bulk_nodes(outputs, NodeType::Output);

        let mut base_geneotype = Geneotype::build_minimal_viable_geneotype(
            input_nodes, 
            output_nodes, 
            self.storage.clone(),
            build_new_random_generator(self.rng_generator.rand_u32(), self.rng_generator.rand_u32())
        );

        let mut storage = self.storage.write().unwrap();
        
        for _i in 0..initial_size.unwrap_or(100) {
            storage.save_geneotype(base_geneotype.create_copy());
        }
    }
}

pub fn build_new_random_generator(high: u32, low: u32) -> Rand32 {
    let new_seed:u64 = ((high as u64) << 32) | (low as u64);
    oorandom::Rand32::new(new_seed)
}

#[cfg(test)]
mod tests {
    #[test]
    fn tests() {
        let x = 3;
        assert_eq!(x,3);
    }

}
