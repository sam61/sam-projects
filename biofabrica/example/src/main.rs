use warp::Filter;


#[tokio::main]
async fn main() {
    println!("Starting Player Commands");
    let example = warp::path!("example" / String / i32)
        .map(|name, age| format!("Example, {}, you are this many years old {}!", name, age));


    warp::serve(example)
        .run(([127,0,0,1], 3031))
        .await;
}
