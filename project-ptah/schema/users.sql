create table user_metadata (
  id serial primary key,
  username varchar not null,
  created timestamptz  not null default 'NOW'::timestamptz,
  passkey varchar not null,
  email varchar
);
