#!/bin/sh

docker run --rm -it --network host -u $(id -u ${USER}):$(id -g ${USER}) -v $(pwd):/working blainehansen/postgres_migrator --pg-url postgresql://sam:password@localhost:5432/ptah generate  $(date +%s%N | cut -b1-13)
docker run --rm -it --network host -u $(id -u ${USER}):$(id -g ${USER}) -v $(pwd):/working blainehansen/postgres_migrator --pg-url postgresql://sam:password@localhost:5432/ptah migrate
