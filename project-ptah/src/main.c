#include <stdlib.h>
#include <stdio.h>

#include <libpq-fe.h>

int main(int argc, char** argv) {
  PGconn* conn = PQconnectdb("postgresql://sam:password@localhost:6432/ptah");

  printf("DB: %s, User: %s\n", PQdb(conn), PQuser(conn));

  PGresult* res = PQexec(conn, "select * from user_metadata");

  printf("Result number: %d\n", PQntuples(res));
  printf("Columns: 1 - %s, 2 - %s, 3 - %s, 4 - %s, 5 - %s\n", 
      PQfname(res, 0), 
      PQfname(res, 1), 
      PQfname(res, 2), 
      PQfname(res, 3), 
      PQfname(res, 4)
  );

  printf("Id: %d, Username: %s, Created: %s, Passkey: %s, Email: %s\n", 
    atoi(PQgetvalue(res, 0, 0)),
    PQgetvalue(res, 0, 1),
    PQgetvalue(res, 0, 2),
    PQgetvalue(res, 0, 3),
    PQgetisnull(res, 0, 4) ? "NULL" : PQgetvalue(res, 0, 4) 
  );

  PQreset(conn);
  return 0;
}
