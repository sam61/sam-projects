
if [ ! -d "include" ]; then
  mkdir include
fi

if [ ! -d "lib" ]; then
  mkdir lib
fi

if [ ! -d "SDL" ]; then
  git clone https://github.com/libsdl-org/SDL
  cd SDL
  mkdir build
  cd build
  cmake -DCMAKE_INSTALL_PREFIX=/usr/local  -DCMAKE_BUILD_TYPE=Release  ..
  cmake --build . --config Release --parallel
  cmake --install . --config Release
  cd ../..
fi

