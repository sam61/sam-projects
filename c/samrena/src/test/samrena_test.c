#include <stdio.h>
#include <assert.h>
#include "../samrena.h"

void print_samrena(Samrena* samrena) {
  
  printf("Arena allocated: %lu\n", samrena->allocated);
  printf("Arena capacity: %lu\n", samrena->capacity);

}

void create_new_arena() {
  Samrena* samrena = samrena_allocate(10);

  print_samrena(samrena);

  int32_t* data = samrena_push_zero(samrena, 400*sizeof(int32_t));


  printf("Address of Data %p\n", (void*) data);
  printf("Address of Samrena? %p\n", (void*) samrena);
  printf("Address of Samrena + sizeof %p\n", (void*) samrena + sizeof(Samrena));

  assert(data != 0);
  assert((void*) data == (void*) samrena + sizeof(Samrena));

  print_samrena(samrena);
  samrena_deallocate(samrena);

}


int main(int argc, char** argv) {

  printf("START SAMRENA TESTING\n");


  int x = 42;
  int* ptr = &x;

  printf("Address of X %p\n", &x);
  printf("pointer? %p\n", (void*) ptr);

  create_new_arena();

  return 0;
}
