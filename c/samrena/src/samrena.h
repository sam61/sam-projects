#ifndef SAMRENA_H
#define SAMRENA_H

#include <stdint.h>
#include <stdlib.h>

typedef struct {
  uint8_t* bytes;
  uint64_t allocated;
  uint64_t capacity;
} Samrena;


Samrena* samrena_allocate(uint64_t page_count);
void samrena_deallocate(Samrena* samrena);

void* samrena_push(Samrena* samrena, uint64_t size);
void* samrena_push_zero(Samrena* samrena, uint64_t size);

uint64_t samrena_allocated(Samrena* samrena);
uint64_t samrena_capacity(Samrena* samrena);

#endif
