;; Don't show the splash screen
(setq inhibit-statup-message t
      visible-bell t
      display-line-numbers 'relative
      )

(tool-bar-mode -1)
(scroll-bar-mode -1)
(display-line-numbers-mode 1)


(load-theme 'modus-vivendi t)
