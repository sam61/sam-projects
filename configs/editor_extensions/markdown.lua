
vim.api.nvim_set_keymap('n', '<Space>mj','vib"hy<Esc>:e <C-r>h<CR>', { noremap = true, silent = true });

vim.api.nvim_set_keymap('n', '<Space>mf','gqap', { noremap = true, silent = true });
vim.cmd [[
filetype plugin on
let g:pencil#wrapModeDefault = 'hard'   " default is 'hard'
let g:pencil#autoformat = 1
let g:pencil#textwidth = 74
augroup pencil
  autocmd!
  autocmd FileType markdown,mkd call pencil#init()
  autocmd FileType text         call pencil#init()
augroup END
]]
