--Terminal!
--Opens terminal in split
vim.api.nvim_set_keymap('n', '<Space>ets',':sp<CR><C-W>K:terminal<CR>:res 35<CR>i', { noremap = true, silent = true });
-- Opens terminal in buffer and renames terminal for easy moving
vim.api.nvim_set_keymap('n', '<Space>etb','::terminal<CR>:f terminal<CR>', { noremap = true, silent = true });
-- Escapes from the terminal, so that you can type again
vim.api.nvim_set_keymap('t', '<Esc>', '<C-\\><C-N>', { noremap = true, silent = true});
vim.api.nvim_set_keymap('t', '<Esc>q', '<C-\\><C-N>:startinsert<CR><C-c><C-c>exit<CR><CR>', { noremap = true, silent = true});
vim.api.nvim_set_keymap('n', '<Esc>q', ':startinsert<CR><C-c><C-c>exit<CR><CR>', { noremap = true, silent = true});
--vim.api.nvim.set_keymap('t', '<Esc><Esc>', '<C-c><C-c>exit<CR><CR>', { noremap = true, silent = true});
