
require('packer').startup(function()
  -- Packer can manage itself
  use 'wbthomason/packer.nvim'
  use {
  'nvim-lualine/lualine.nvim',
  requires = { 'kyazdani42/nvim-web-devicons', opt = true }
  }
  -- Optional
  use 'nvim-telescope/telescope.nvim'
  use 'mhartington/oceanic-next'
  use 'p00f/nvim-ts-rainbow'
  use 'benknoble/vim-racket'
  use 'glepnir/oceanic-material'
  use 'neovim/nvim-lspconfig'
  --use 'nvim-lua/completion-nvim'
  use 'preservim/nerdtree'
  use 'lifepillar/pgsql.vim'
  use 'rust-lang/rust.vim'
  use 'simrat39/rust-tools.nvim'
  use 'kamykn/spelunker.vim'
  use 'terrastruct/d2-vim'
  use 'nvim-lua/plenary.nvim'
  use 'mfussenegger/nvim-dap'
  use {
	'ms-jpq/coq_nvim',
	branch = 'coq'
  }
  use {
	'ms-jpq/coq.artifacts',
	branch = 'artifacts'
  }
  use 'preservim/vim-pencil'
  use {
        'nvim-treesitter/nvim-treesitter',
        run = ':TSUpdate'
    }
  use 'nvim-treesitter/playground'
  use 'vim-scripts/dbext.vim'
end)


local g = vim.g
local opt = vim.opt
local cmd = vim.api.nvim_command
local api = vim.api
opt.termguicolors = true
opt.colorcolumn = "79"
opt.syntax = "ON"
g.background = "dark"
vim.o.expandtab = true;
vim.o.tabstop = 2;
vim.o.shiftwidth = 2;
vim.wo.number = true;
vim.wo.relativenumber = true;


vim.g.mapleader = ' '

--FILE EXPLORER

local function t(str)
    -- Adjust boolean arguments as needed
    return vim.api.nvim_replace_termcodes(str, true, true, true)
end

vim.api.nvim_set_keymap('n', '<Space>p',':bn<CR>', { noremap = true, silent = true });
vim.api.nvim_set_keymap('n', '<Space>o',':bp<CR>', { noremap = true, silent = true });
vim.api.nvim_set_keymap('n', '<Space>u',':bp|bd#<CR>', { noremap = true, silent = true });
vim.api.nvim_set_keymap('n', '<Space>z','Zg', { noremap = false, silent = true });
vim.api.nvim_set_keymap('n', '<Space>ew',':Explore<CR>', { noremap = true, silent = true });
vim.api.nvim_set_keymap('n', '<Space>ev',':vsp<CR>:Explore<CR>', { noremap = true, silent = true });
vim.api.nvim_set_keymap('n', '<Space>es',':sp<CR>:Explore<CR>', { noremap = true, silent = true });
vim.api.nvim_set_keymap('n', '<Space>so',':so %<CR>', { noremap = true, silent = true });
--vim.cmd("let NERDTreeIgnore=['\.o$']")


--Terminal!
--vim.api.nvim_set_keymap('n', '<Space>et',':sp<CR><C-W>K:terminal<CR>:res 35<CR>i', { noremap = true, silent = true });
--vim.api.nvim_set_keymap('t', '<Esc>', '<C-\\><C-N><C-w><C-j><C-w><C-l>', { noremap = true, silent = true});
--vim.api.nvim_set_keymap('t', '<Esc>q', '<C-\\><C-N>:startinsert<CR><C-c><C-c>exit<CR><CR>', { noremap = true, silent = true});
--vim.api.nvim_set_keymap('n', '<Esc>q', ':startinsert<CR><C-c><C-c>exit<CR><CR>', { noremap = true, silent = true});
--vim.api.nvim.set_keymap('t', '<Esc><Esc>', '<C-c><C-c>exit<CR><CR>', { noremap = true, silent = true});

--Moving lines with keys
vim.api.nvim_set_keymap('n', '<Space>k',':m-2<CR>', { noremap = true, silent = true });
vim.api.nvim_set_keymap('n', '<Space>j',':m+1<CR>', { noremap = true, silent = true });


cmd("au BufNewFile,BufRead,BufReadPost *.h setfiletype c");
cmd("au BufNewFile,BufRead,BufReadPost *.rgbasm setfiletype rgbasm");
cmd("colorscheme OceanicNext")
cmd("let g:sql_type_default = 'pgsql'");
cmd("let g:dbext_default_type = 'PG'");
cmd("let g:dbext_default_user = 'sam'");
cmd("let g:dbext_default_password = 'password'");
cmd("let g:dbext_default_profile_PG = 'type=PGSQL:user=sam'");
--cmd("setlocal spell spelllang=en_au");
cmd("set nospell");
cmd("autocmd FileType c setlocal colorcolumn=80");
local nvim_lsp = require('lspconfig')
local on_attach = function(client, bufnr)
  --require('completion').on_attach()

  local function buf_set_keymap(...) vim.api.nvim_buf_set_keymap(bufnr, ...) end
  local function buf_set_option(...) vim.api.nvim_buf_set_option(bufnr, ...) end
  
  client.server_capabilities.documentFormattingProvider = true
  buf_set_option('omnifunc', 'v:lua.vim.lsp.omnifunc')

  -- Mappings
  local opts = { noremap=true, silent=true }
  buf_set_keymap('n', 'gD', '<Cmd>lua vim.lsp.buf.declaration()<CR>', opts)
  buf_set_keymap('n', 'gd', '<Cmd>lua vim.lsp.buf.definition()<CR>', opts)
  buf_set_keymap('n', 'K', '<Cmd>lua vim.lsp.buf.hover()<CR>', opts)
  buf_set_keymap('n', 'gi', '<cmd>lua vim.lsp.buf.implementation()<CR>', opts)
  buf_set_keymap('n', '<C-k>', '<cmd>lua vim.lsp.buf.signature_help()<CR>', opts)
  buf_set_keymap('n', '<space>wa', '<cmd>lua vim.lsp.buf.add_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wr', '<cmd>lua vim.lsp.buf.remove_workspace_folder()<CR>', opts)
  buf_set_keymap('n', '<space>wl', '<cmd>lua print(vim.inspect(vim.lsp.buf.list_workspace_folders()))<CR>', opts)
  buf_set_keymap('n', '<space>D', '<cmd>lua vim.lsp.buf.type_definition()<CR>', opts)
  buf_set_keymap('n', '<space>rn', '<cmd>lua vim.lsp.buf.rename()<CR>', opts)
  buf_set_keymap('n', 'gr', '<cmd>lua vim.lsp.buf.references()<CR>', opts)
  buf_set_keymap('n', '<space>e', '<cmd>lua vim.lsp.diagnostic.show_line_diagnostics()<CR>', opts)
  buf_set_keymap('n', '[d', '<cmd>lua vim.lsp.diagnostic.goto_prev()<CR>', opts)
  buf_set_keymap('n', ']d', '<cmd>lua vim.lsp.diagnostic.goto_next()<CR>', opts)
  buf_set_keymap('n', '<space>q', '<cmd>lua vim.lsp.diagnostic.set_loclist()<CR>', opts)
  buf_set_keymap("n", "<Space>f", "<cmd>lua vim.lsp.buf.format({async = true})<CR>", opts)
  -- Set some keybinds conditional on server capabilities
end


local servers = {"clangd", "ts_ls", "rust_analyzer", "eslint", "svlangserver", "yamlls"}
for _, lsp in ipairs(servers) do
    nvim_lsp[lsp].setup {
      on_attach = on_attach,
    }
end

nvim_lsp.yamlls.setup{
  yaml = {
    schemaStore = {
      enable = true
   }
}}

local rt = require("rust-tools")

rt.setup({
  server = {
    on_attach = function(_, bufnr)
      -- Hover actions
      vim.keymap.set("n", "<C-space>", rt.hover_actions.hover_actions, { buffer = bufnr })
      -- Code action groups
      vim.keymap.set("n", "<Leader>a", rt.code_action_group.code_action_group, { buffer = bufnr })
    end,
  },
})

require('lualine').setup {
  options = {
    icons_enabled = false,
    theme = 'OceanicNext',
    component_separators = { left = '', right = ''},
    section_separators = { left = '', right = ''},
    disabled_filetypes = {},
    always_divide_middle = true,
    globalstatus = false,
  },
  sections = {
    lualine_a = {'mode'},
    lualine_b = {'branch', 'diff'},
    lualine_c = {},
    lualine_x = {'encoding', 'filetype'},
    lualine_y = {'progress'},
    lualine_z = {'location'}
  },
  inactive_sections = {
    lualine_a = {},
    lualine_b = {},
    lualine_c = {'filename'},
    lualine_x = {'location'},
    lualine_y = {},
    lualine_z = {}
  },
  tabline = {
  	lualine_a = {{
   	'buffers',
  	mode=2,
  	symbols = {
    	modified =  ' +',
    	alternative_file = '',
    	directory = ' +-',
  	}}},
  lualine_b = {},
    lualine_c = {},
  lualine_x = {},
  lualine_y = {},
  lualine_z = {'tabs'}
  },
  extensions = {}
}

require'nvim-treesitter.configs'.setup {
  -- A list of parser names, or "all"
  ensure_installed = { "c", "lua", "typescript", "javascript", "rust", "python", "sql", "verilog", "yaml", "scheme"},

  -- Install parsers synchronously (only applied to `ensure_installed`)
  sync_install = false,

  -- List of parsers to ignore installing (for "all")
  --ignore_install = { "javascript" },

  highlight = {
    -- `false` will disable the whole extension
    enable = true,

    -- NOTE: these are the names of the parsers and not the filetype. (for example if you want to
    -- disable highlighting for the `tex` filetype, you need to include `latex` in this list as this is
    -- the name of the parser)
    -- list of language that will be disabled
    --disable = {  "rust" },

    -- Setting this to true will run `:h syntax` and tree-sitter at the same time.
    -- Set this to `true` if you depend on 'syntax' being enabled (like for indentation).
    -- Using this option may slow down your editor, and you may see some duplicate highlights.
    -- Instead of true it can also be a list of languages
    additional_vim_regex_highlighting = true,
  },
  rainbow = {
    enable = true,
    -- disable = { "jsx", "cpp" }, list of languages you want to disable the plugin for
    extended_mode = true, -- Also highlight non-bracket delimiters like html tags, boolean or table: lang -> boolean
    max_file_lines = nil, -- Do not enable for files with more than n lines, int
    -- colors = {}, -- table of hex strings
   -- -- termcolors = {} -- table of colour name strings
  }
}

require "editor_extensions.load_order"
