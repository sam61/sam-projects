
# init lua
mkdir -p ~/.config/nvim && ln -sf "$PWD/init.lua" ~/.config/nvim/init.lua
mkdir -p ~/.config/nvim/lua/editor_extensions 

ln -sf "$PWD/editor_extensions/load_order.lua" ~/.config/nvim/lua/editor_extensions/load_order.lua
ln -sf "$PWD/editor_extensions/terminal.lua" ~/.config/nvim/lua/editor_extensions/terminal.lua
ln -sf "$PWD/editor_extensions/markdown.lua" ~/.config/nvim/lua/editor_extensions/markdown.lua
ln -sf "$PWD/editor_extensions/scheme.lua" ~/.config/nvim/lua/editor_extensions/scheme.lua

mkdir -p ~/.config/nvim/syntax && ln -sf "$PWD/syntax/rgbasm.vim" ~/.config/nvim/syntax/rgbasm.vim

# Init emacs

mkdir -p ~/.config/emacs && ln -sf "$PWD/init.el" ~/.config/emacs/init.el
