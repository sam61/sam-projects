# The plan

## Current Task

Initial setup of Stock Trading.

### Timeframe

Finished by October 1st

### Pomodoro numbers

Achieve within 40. List pomodoro sheets inside of project folder

### Goals:

1. Export a Latex Report (No graphics needed, CSV basically)
2. Collect daily data for all stocks 
3. Perform basic stock chart techniques 
4. Have it testable to show it works 
5. Versionable trading strategies

## Key Points

I forever have a list of things I want to do. This list will never end. So
rather than go crazy trying to do all of them I will do as Julian (work
friend) suggested and timebox myself around one. Whenever I think of
something I will add it to the list and when I come around to finshing
a task I will pick a new one.

The idea is I should eventually start finishing things and maybe be able
to add things.

I will place this into Pomodoro to give myself a set number I can use.

## Why do this

I find myself questioning my decisions constantly. 

I also get nothing done. So you see the problem!
