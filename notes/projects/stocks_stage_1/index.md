# Stock stage one


## Goal

1. Output a latex report of trades and reasoning (no graphs needed yet) 
2. collect data on a daily basis, recover missing days 
3. Calculate chart algorithms (rsi, macd, moving averages) 
4. Create initial trading algo and version it
5. Benchmark the algo and report it

## Modules

### Report

### Algo

### Data collections/stores
