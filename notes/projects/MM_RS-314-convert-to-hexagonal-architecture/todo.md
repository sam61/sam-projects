# TODO

This is a massive piece of work. I'd say quite massive in fact.

I need to re-organize Report-Services as a Hexagonal architecture which
has been a little hard to tell you the truth.

Not hard like it is a difficult problem, but like 'everything, everywhere,
all at once' kind of changes.

## Ultimate 'done'

- Can compile
- Original tests passing
- PR passes


## What do I want to complete by COB 

- All ports actually done
- No errors in the external code
