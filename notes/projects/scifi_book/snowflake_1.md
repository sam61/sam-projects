# Step One 

Take an hour and write a one-sentence summary of your novel. Something
like this: “A rogue physicist travels back in time to kill the apostle
Paul.” (This is the summary for my first novel, Transgression.) 

Shorter is better. Try for fewer than 15 words.

Tie together the big picture and the personal picture. Which character 
has the most to lose in this story? Now tell me what he or she wants to
win.

# Step Two

expand that sentence to a full paragraph describing the story setup, 
major disasters, and ending of the novel.


# Step Three

For each of your major characters, take an hour and write a one-page
summary sheet that tells.


- The character’s name 
- A one-sentence summary of the character’s storyline 
- The character’s motivation (what does he/she want abstractly?) 
- The character’s goal (what does he/she want concretely?) 
- The character’s conflict (what prevents him/her from reaching this
  goal?) 
- The character’s epiphany (what will he/she learn, how will he/she
  change? 
- A one-paragraph summary of the character’s storyline

