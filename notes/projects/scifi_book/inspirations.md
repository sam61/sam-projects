
# Ending

Should be taking over from the Nanomachines, should appear on the surface
that the robot is an improvement but really its just keeping the torture
going

ie, the humans who are 'alive' want the robot to disable the nanomachines
to let them die but he is "I can't let you do that" at the end.

Should be implied that this has happened before.

Story within a story should imply that as well.

# Story within a story

The Grasshopper Lies Heavy, which was written by the Man in the High
Castle. As Dick's novel details a world in which the Axis Powers of World
War II had succeeded in dominating the known world, the novel within the
novel details an alternative to this history in which the Allies overcome
the Axis and bring stability to the world – a victory which itself is
quite different from real history. 
