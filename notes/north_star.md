# My north star goals.

Last reviewed: 2024-09-12

## Health

I want to be under a BMI of 35 

I want to be able to jog

## Completion

I want to start and finish a personal project

I want to be proud of my work

## Financial Independence

I to be able to not worry about money

I want to be able to do taxes by myself

I would like to retire

## Business Ownership

I want to work for myself

I want to make something for myself
