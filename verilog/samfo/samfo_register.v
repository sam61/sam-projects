

module samfo_register
#(parameter 
  register_width = 4, 
  bus_reg_length = 2, 
  bus_number = 1
)
(
  input   wire                      enqueue,  //Shift data out and take prev_incoming
  input   wire                      save,     //Save data from the bus
  input   wire[2:0]                 save_bus, //value specific to register used to save on bus input
  input   wire[register_width-1:0]  bus_incoming,      
  input   wire[register_width-1:0]  prev_incoming,      
  output  reg[register_width-1:0]   data, 
  output  reg                       contains_data
);

always @(posedge enqueue) begin


end

endmodule
