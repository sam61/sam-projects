import { useState } from "react";
import { WindowManagerState } from "./types/windows";

export default function WindowManager() {
  const [curId, title] = useState<WindowManagerState>({ curId: 0, title: "hello" });

  console.log(curId, title);
}
