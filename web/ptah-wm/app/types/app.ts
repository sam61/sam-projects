export type App = {
  id: number;
  x: number;
  y: number;
  z: number;
  width: number;
  height: number;
};
