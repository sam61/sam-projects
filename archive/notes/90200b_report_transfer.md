
# Index

Report transfer of referrals between practionners and patients.

For a practioner to have access to a report the referral record should be
owned by the same practitioner. Also the patient should be linked to the
practioner.

This one looks like it can be done entirely within the CORE Api

## Questions to ask

Do we want to make the practioners many to one patient?

# Card

1. Get ID of the pracitioner (needs email and/or full name)

2. To transfer, update the practioner service record_id in referral table
   with the id we got before (this will replace)

3. Practioner_to_patient table transfer by creating a practitioner service
   record



