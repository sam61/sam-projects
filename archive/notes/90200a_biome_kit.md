

# Task

Create a kanban board entry for the Biome Kit replacement

Need to know if we 100% need two DBs

## Index

[90200a1 - OT3 Backend Renaming Docs](90200a1_0t3_docs.md)

# Kanban over view

Ticket 1:

Create API endpoint

This endpoint will take in the necessary information to create this. From
the looks of it we just need the sample idenitfier. But if that isn't
easily gettable we will need to get it from the referral reference.

Ticket 2:

SQL Query Insert

Simple - Replace the query and add it to the the DB queries. It has been
well built.

I doubt there is any need to do bulk updates? Ask in the meeting.

Ticket 3:

Recreate process of ordering kit.

1. Get shopify order ID from referral table from the hcp-journey database
   (referral_reference from kit table can be used as the id of the
   referral record)

2. Get bearer token. Uses the auth-api.microba.com/oauth/token endpoint.
   Possibly able to get a token directly, or we an API thing.

3. Push to the SQS shopify bridge https://api.microba.com/admin/sqs/shopify-data-bridge-topic-prod

4. Make sure that the kit prepared for replacement has its data sorted out
   in the end. May require callnback 


