
# Harvest Ant Project

## Dates

### When do I hope to have this finished?

Ideal goal would be the 22/09/23 and that would probably be unrealistic.

### Start

20/09/23

### End

### Length of time taken

1. 20/09/23 - XX hours

## What is this?

Adam asked me to work on the Harvest Ant google cloud project and replace
the broken MQTT IOT core system with a broker.

## Process taken to solve this

### Document read 

From the looks of things it is super simple, I have a device that needs an
MQTT host address. Set that up, link to PUB/SUB on google compute and pray
that it works???

### The Google

Found HiveMQ which looked promising as a backup.

Instead after some googling I found Eclipise Mosquitto which Adam and Tim
had planned on using. So it turns out that I did a smart thing and worked
it out! Yay

Very happy with that. Found a Docker Image, now I just need to start
working on the solution. Will load up the MQTT library on my computer and
start moving stuff around.

### Current process idea 20/09/23

1. Start with getting a rest request to PUB/SUB going. 
2. Conect gateway to MQTT Mosquitto and dump messages 
3. Try and connect the two 
4. Once that is done I will test. 

This is probably way too easy

I was right, I don't know what the message looks like. Will go in reverse
order now.

1. Connect gateway to MQTT Mosquitto and dump messages


