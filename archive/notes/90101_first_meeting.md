Kit replacement

- Practioner will send a referral to replace a kit, and it will be handled
  by shopify. Shopify will send a purchase url to the user who will buy.
  The order will be fulfiled by a backend person. They need to automate
  replacements.
    - Has issues with speed of return as samples need to be kept cool


- Patient report transfer Build on the assumption that one patient sees
  a single practioner. In reality one patient can have multiple
  practioners. Need a way to refer customers to other practioners. 
  - Across clinic issues. 

- Resend referral notification. SMS sends shopify purchase link, but may
  need to be resend referall. If customer loses it they need a resend for
  a customer. Around 5/per week to do a resend.

- Practioner Register and password reset - Manually managed. Team in
  Pakistan that build the CRM that has a form that links to the practioner
  website. This form registers interests and requires teh user to list
  relevant information. When approved their login records can be created
  in the portal ideally, but that will require integration with the CRM.
  Need the process approval automated.
  - Need to redesign password, but I don't think we need to handle this.
  - Authentication handled by Auth (What one - Oauth I think). 

- Dispatch Portal stuff - Address corrections, stop dispatching kid,
  manifest generation. Shopify is used for everything at the moment.
  Shopify doesnt have good address validation. Auspost API has been very
  good for them in the past. ShopifyPlus is something they want to avoid
  due to the price of it. 
  - Currently not handling 500 errors from AusPost very well at the moment

- Data fixing. Phone number, emails, and other changes. There are
  complications when these are fixed. Must be fixed by hand at the moment. 

- Cloning data between services so it can be used for testing purposes.
  For exmaple, a sample will go to the test product.

- Manufacturing Kits -> many parts need to be assembled before dispatch.
  I think they want to do a manifest check and pre assembling so they can
  be stocked in practioners and warehouses or even a pharmacy.

- Microsoft Dynamic CRM

