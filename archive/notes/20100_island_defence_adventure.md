# Island Defence Adventure

## Name

No idea what the name should be. 

## Index

[20110 - Technology](20110_island_defence_technology.md)

## About the game

### In a sentence

You must take control of a technological backwards island nation and drive
off colonizers coming to steal your land and enslave your people.

### Summary

You are a single person on an island city state in an alciphelago. One day
some conquistador esq invaders come to claim the island in the name of
their Queen/King/Chancellor or whatever. You have to recruit an army,
build a base and work towards throwing the invaders back. This is
a multi-year affair which will involve farming, building forts, research,
sneak attacks before finally defeating the governor and retacking the
city.

## Features

### Map

### Building

### Leadership

### Survival

### Combat

### Tech???

Need to choose between C and Rust.


