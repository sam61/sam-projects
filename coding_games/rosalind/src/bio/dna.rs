
use std::str;

pub struct DNA {
    raw_dna: String,
    adenine_count: i32,
    cytosine_count: i32,
    guanine_count: i32,
    thymine_count: i32
}

pub fn dna_complement(nucleobase: char) -> char {
    match nucleobase {
        'A' => 'T',
        'T' => 'A',
        'C' => 'G',
        'G' => 'C',
        _ => 'C'
    }
}

pub fn dna_match(nucleobase: &char) -> bool {
    match nucleobase {
        'A' => true,
        'T' => true,
        'C' => true,
        'G' => true,
        _ => false
    }
}

impl DNA {
    
    fn count_nucleobases(&mut self) {
        for nucleobase in self.raw_dna.chars() {
            match nucleobase {
                'A' => self.adenine_count += 1,
                'C' => self.cytosine_count += 1,
                'G' => self.guanine_count += 1,
                'T' => self.thymine_count += 1,
                _ => ()
            }
        }
    }

    pub fn new(raw_dna: &String) -> DNA {
       
        let mut new_dna: DNA = DNA {
            raw_dna: raw_dna.clone(),
            adenine_count: 0,
            cytosine_count: 0,
            guanine_count: 0,
            thymine_count: 0
        };

        new_dna.count_nucleobases();
        new_dna
    }

    pub fn print_nucleobases(&self) {
        println!("{} {} {} {}",
            self.adenine_count,
            self.cytosine_count,
            self.guanine_count,
            self.thymine_count
        );
    }

    pub fn get_rna_string(&self) -> String {
       str::replace(&self.raw_dna, "T", "U") 
    }

    pub fn get_complement_string(&self) -> String {
        self.raw_dna.chars().filter(|x| dna_match(x)).map(|x| dna_complement(x)).collect() 
    }

    pub fn get_complement_string_reversed(&self) -> String {
        self.get_complement_string().chars().rev().collect()
    }

}

