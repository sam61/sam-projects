
use std::str;

pub struct RNA {
    raw_rna: String,
    adenine_count: i32,
    cytosine_count: i32,
    guanine_count: i32,
    uracil_count: i32
}

pub fn rna_complement(nucleobase: char) -> char {
    match nucleobase {
        'A' => 'U',
        'U' => 'A',
        'C' => 'G',
        'G' => 'C',
        _ => 'C'
    }
}

pub fn rna_match(nucleobase: &char) -> bool {
    match nucleobase {
        'A' => true,
        'U' => true,
        'C' => true,
        'G' => true,
        _ => false
    }
}

impl RNA {
    fn count_nucleobases(&mut self) {
        for nucleobase in self.raw_rna.chars() {
            match nucleobase {
                'A' => self.adenine_count += 1,
                'C' => self.cytosine_count += 1,
                'G' => self.guanine_count += 1,
                'U' => self.uracil_count += 1,
                _ => ()
            }
        }
    }

    pub fn new(raw_rna: &String) -> RNA {
       
        let mut new_rna: RNA = RNA {
            raw_rna: raw_rna.clone(),
            adenine_count: 0,
            cytosine_count: 0,
            guanine_count: 0,
            uracil_count: 0
        };

        new_rna.count_nucleobases();
        new_rna
    }

    pub fn print_nucleobases(&self) {
        println!("{} {} {} {}",
            self.adenine_count,
            self.cytosine_count,
            self.guanine_count,
            self.uracil_count
        );
    }

    pub fn get_dna_string(&self) -> String {
       str::replace(&self.raw_rna, "U", "T") 
    }

    pub fn get_complement_string(&self) -> String {
        self.raw_rna.chars().filter(|x| rna_match(x)).map(|x| rna_complement(x)).collect() 
    }

    pub fn get_complement_string_reversed(&self) -> String {
        self.get_complement_string().chars().rev().collect()
    }

}

