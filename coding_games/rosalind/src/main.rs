
use std::fs;
use std::error::Error;
use std::process::exit;
use std::env;

pub mod bio;

use bio::dna::DNA;
use bio::rna::RNA;

fn hamming_distance(first: &str, second: &str) -> Result<u32, String> {
    
    if first.len() != second.len() {
        return Err("Please ensure that the strings are the same length".to_string())
    }

    let mut first_chars = first.chars();
    let mut second_chars = second.chars();
    let mut count = 0;
    while let Some(i) = first_chars.next() {
        let j = second_chars.next().unwrap();
        
        if i != j {
            count += 1;
        }
    }

    Ok(count)
}

fn challenge_hamm(args: &Vec<String>) {
    
    if args.len() < 4 {
        println!("Please include two equal length strings");
        exit(-1);
    }

    let count = match hamming_distance(&args[2], &args[3]) {
        Ok(i) => i,
        Err(i) => {
            println!("{}", i); 
            exit(-2)
        }
    };

    println!("Hamming Count: {}", count);
}

fn challenge_dna(args: &Vec<String>) {
    
    if args.len() < 3 {
        println!("Please include a file");
        exit(-1);
    }
    
    let raw_dna = fs::read_to_string(&args[2]).unwrap();
    let dna: DNA = DNA::new(&raw_dna);
    dna.print_nucleobases();
}

fn challenge_rna(args: &Vec<String>) {
    
    if args.len() < 3 {
        println!("Please include a file");
        exit(-1);
    }
    
    let raw_dna = fs::read_to_string(&args[2]).unwrap();
    let dna: DNA = DNA::new(&raw_dna);
    println!("{}", dna.get_rna_string());
}

fn challenge_revc(args: &Vec<String>) {
    
    if args.len() < 3 {
        println!("Please include a file");
        exit(-1);
    }
    
    let raw_dna = fs::read_to_string(&args[2]).unwrap();
    let dna: DNA = DNA::new(&raw_dna);
    println!("{}", dna.get_complement_string_reversed());
}

fn help() {
    println!("This is rosalind challengs. Use the name of the challenge to run it");
    exit(-1);
}

fn main() {
    println!("Hello, world!");

    let args: Vec<String> = env::args().collect();

    if args.len() < 2 {
        help();  
    }

    println!("{:?}", args);

    match args[1].as_str() {
        "dna"   => challenge_dna(&args),
        "rna"   => challenge_rna(&args),
        "revc"  => challenge_revc(&args),
        "hamm"  => challenge_hamm(&args),
        _           => help(),
    }
}
